<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a id="logo" href="/home"><img class="standard"
                                           src="//www.vivifyideas.com/wp-content/uploads/2014/07/logo1.png"
                                           alt="Vivify Ideas | Web Application Development Company"></a>
        </div>
        <div>
            <ul class="nav navbar-nav">
                <li><a @yield('nav-active-home') href="/home">Home</a></li>
                <li><a @yield('nav-active-contact') href="/contact">Contact</a></li>
            </ul>
        </div>
        <div>
            <?php
            if (null !== Auth::user()) {
                $userName = 'Welcome ' . Auth::user()->firstname . ' ' . Auth::user()->lastname;
                $logout = '<div class="dropdown logout">
                                <button class="dropdown-toggle" type="button" data-toggle="dropdown">'
                                    . $userName .
                                    '<span class="caret"></span>
                                </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <a href="/auth/logout">
                                                Logout
                                            </a>
                                        </li>
                                    </ul>
                           </div>';
                echo $logout;
            }
            ?>
        </div>
    </div>
</nav>