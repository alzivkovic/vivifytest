<html ng-app="vivify">

<!-- <div class="alert alert-success fade in">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> Indicates a successful or positive action.
</div> -->

<div class="todo-wrapper todos-table" ng-controller="todoController">
    <h2>To Do List Management</h2>
    <h4>You've got <span class="emphasis"><%getTotalTodos()%></span> things to do</h4>
    <br/>
    <table class="table-striped table-bordered table-hover custom-table">
        <tr>
            <th class="td-check">
                Done
            </th>
            <th>
                Task name
            </th>
            <th class="priority-td">
                Task priority
            </th>
            <th colspan="3">
                Todo Actions
            </th>
        </tr>
        <tr ng-repeat="todo in todos track by todo.id" class="<%todo.modified%>" ng-click="setActive()">
            <td class="td-check <%todo.active%>">
                <input class="form-control" type="checkbox" ng-model="todo.done" ng-click="checkIfModified()"/>
            </td>
            <td class="td-text <%todo.active%>">
                <%todo.name%>
            </td>
            <td class="td-priority <%todo.active%>">
                <select class="form-control" ng-model="todo.priority" ng-click="checkIfModified()">
                    <option value="1">Low</option>
                    <option value="2">Medium</option>
                    <option value="3">High</option>
                </select>
            </td>
            <td>
                <button class="btn btn-info" ng-click="modifyName()"> Modify Name
                </button>
            </td>
            <td>
                <button class="btn btn-success" ng-click="saveTodo()"> Save Changes
                </button>
            </td>
            <td>
                <button class="btn btn-danger" ng-click="removeTodo()"> Delete Todo
                </button>
            </td>
        </tr>
    </table>
    <br />
    <button class="btn btn-warning" ng-click="discardChanges()"></span>
        Discard Changes
    </button>
    <hr/>
    Change Task Name:
    <input class="form-control" placeholder="Select Task to modify name..." type="text"
           ng-model="formTodoTextModify">
    <br/>

    <hr/>
    New Task Creator
    <input class="form-control" placeholder="Type new task name..." type="text" ng-model="formTodoText"/>

    <br/>
    Chose Priority:
    <select class="form-control" ng-options="option.name for option in priority.availableOptions track by option.id"
            ng-model="priority.selectedOption"></select>

    <br/>
    <br/>
    <button class="btn btn-primary btn-group-justified" ng-click="addTodo()"> Add New Task
    </button>
    <hr/>
</div>

</html>