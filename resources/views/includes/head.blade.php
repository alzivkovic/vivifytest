<meta charset="utf-8">
<meta name="description" content="">
<meta name="author" content="Scotch">

<title>Vivify Ideas</title>

<!-- load bootstrap from a cdn -->
<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/jquery-2.1.4.min.js"></script>
<script src="/js/bootstrap.min.js"></script>

<script>var inScriptTagVar = 'setted value';</script>
<script src="/js/beforeExample.js"></script>
<script src="/js/example.js"></script>
<script src="/js/afterExample.js"></script>
<script src="/js/user.js"></script>

<link type="text/css" rel="stylesheet" href="/css/testAngular.css"/>
<link type="text/css" rel="stylesheet" href="/css/base.css"/>
<script src="/js/angular.min.js"></script>
<script src="/js/vivify.js"></script>
<link rel="shortcut icon" href="/favicon.ico">
