<!-- resources/views/auth/login.blade.php -->
@extends('layouts.default')
@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <table class="login">
        <tr>
            <td>
                <form method="POST" action="/auth/login">
                    {!! csrf_field() !!}
                    <?php echo csrf_field(); ?>
                        <tr>
                            <td>Email</td>
                            <td><input class="form-control" type="email" name="email" value="{{ old('email') }}"></td>
                        </tr>
                        <tr>
                            <td>Password</td>
                            <td><input class="form-control" type="password" name="password" id="password"></td>
                        </tr>
                        <tr>
                            <td>Remember Me</td>
                            <td><input class="form-control remember-me-check" type="checkbox" name="remember"></td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-primary" type="submit">Login</button>
                            </td>
                        </tr>
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form method="GET" action="/auth/register">
                    <div>
                        <button class="btn btn-primary" type="submit">Sign Up</button>
                    </div>
                </form>
            </td>
        </tr>
    </table>
@stop
