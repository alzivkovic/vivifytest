<!-- resources/views/auth/register.blade.php -->
@extends('layouts.default')
@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<form method="POST" action="/auth/register">
    {!! csrf_field() !!}
    <table class="register">
    <tr>
        <td>Email</td>
        <td><input type="email" name="email" value="{{ old('email') }}"></td>
    </tr>
    <tr>
        <td>Password</td>
        <td><input type="password" name="password"></td>
    </tr>
    <tr>
        <td>Confirm Password</td>
        <td><input type="password" name="password_confirmation"></td>
    </tr>
    <tr>
        <td>First Name</td>
        <td><input type="text" name="firstname" value="{{ old('firstname') }}"></td>
    </tr>
    <tr>
        <td>Last Name</td>
        <td><input type="text" name="lastname" value="{{ old('lastname') }}"></td>
    </tr>
    <tr>
        <td>Company</td>
        <td><input type="text" name="company" value="{{ old('company') }}"></td>
    </tr>
    <tr>
        <td>Country</td>
        <td><select name="country" class="form-control">
             <?php
                $Countries = App\Country::get();
                 foreach($Countries as $Country){
                     echo '<option value="'.$Country->id.'">'.$Country->name.'</option>';
                 }
             ?>
        </select></td>
    </tr>
    <tr>
        <td><button class="btn btn-primary" type="submit">Register</button></td>
    </tr>
    </table>
</form>
@stop