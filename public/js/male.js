"use strict";

class Male extends Person {
    constructor (firstname, lastname) {
        super(firstname, lastname);
        this.gender = 'Male';
    }
}