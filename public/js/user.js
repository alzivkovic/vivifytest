/**
 * Created by vivify on 16.10.15..
 */
var Person = function Person(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
};
Person.prototype = {
    getFirstname: function(){
        return this.firstname;
    },
    getLastname: function(){
        return this.lastname;
    }
};
Person.stat = function(){
    return 'asd';
};

var User = function User(firstname, lastname, country, company) {
    Person.call(this, firstname, lastname);
    this.country = country;
    this.company = company;
};

User.prototype = Object.create(Person.prototype);
User.prototype.constructor = User;