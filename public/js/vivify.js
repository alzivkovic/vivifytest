/**
 * Created by vivify on 19.10.15..
 */
var vivify = angular.module('vivify', []);

vivify.config(function ($interpolateProvider) {

    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

});

vivify.controller("todoController", function ($scope, todoService) {
        $scope.todos = [];

        loadRemoteData();

        $scope.addTodo = function () {
            if (this.formTodoText) {
                var todoData = {
                    name: this.formTodoText,
                    done: false,
                    priority: this.priority.selectedOption.id
                };
                this.formTodoText = '';
                todoService.addTodo(todoData)
                    .then(function($data){
                        $scope.todos.push($data);
                    });
            } else {
                alert("You must type the name of the Task!");
            }
        };

        $scope.removeTodo = function () {
            todoService.removeTodo(this.todo.id)
                .then(function (id) {
                    var newTodosList = [];
                    $scope.todos.forEach(function (todo) {
                        if (todo.id != id) {
                            newTodosList.push(todo);
                        }
                    });
                    $scope.todos = angular.copy(newTodosList);
                });
        };

        $scope.saveTodo = function () {
            if (this.todo.modified) {
                todoService.saveTodo({
                    done: this.todo.done,
                    name: this.todo.name,
                    priority: this.todo.priority
                }, this.todo.id).then(function(id){
                    $scope.todos.forEach(function(todo){
                       if(todo.id == id){
                           delete todo.modified;
                       }
                    });
                    $scope.baseStateTodos = angular.copy($scope.todos);
                });
            } else {
                alert('Nothing to save.');
            }
        };

        function applyRemoteData(newTodos) {
            newTodos.forEach(function (todo) {
                todo.done = todo.done == 1;
            });
            $scope.todos = newTodos;
            $scope.baseStateTodos = angular.copy(newTodos);
            $scope.formTodoTextModify = '';
        }

        function loadRemoteData() {
            todoService.getTodos()
                .then(
                function (todos) {
                    applyRemoteData(todos);
                }
            );
        }

        $scope.priority = {
            availableOptions: [
                {id: '1', name: 'Low'},
                {id: '2', name: 'Medium'},
                {id: '3', name: 'High'}
            ],
            selectedOption: {id: '1', name: 'Low'}
        };


        $scope.getTotalTodos = function () {
            return $scope.todos.length;
        };

        $scope.checkIfModified = function (activeTodo) {
            activeTodo = this.todo || activeTodo;
            $scope.baseStateTodos.forEach(function (todo) {
                if (todo.id == activeTodo.id) {
                    if (todo.done != activeTodo.done || todo.name != activeTodo.name || todo.priority != activeTodo.priority) {
                        activeTodo.modified = 'modified';
                    } else {
                        delete activeTodo.modified;
                    }
                }
            });
        };

        $scope.setActive = function () {
            this.todos.forEach(function (todo) {
                delete todo.active;
            });
            this.todo.active = 'active';
            $scope.formTodoTextModify = this.todo.name;
        };

        $scope.modifyName = function () {
            if (this.formTodoTextModify) {
                var hasSelected = false;
                this.todos.forEach(function (todo) {
                    if (todo.active) {
                        todo.name = $scope.formTodoTextModify;
                        delete todo.active;
                        this.formTodoTextModify = '';
                        $scope.checkIfModified(todo);
                        hasSelected = true;
                    }
                });
                if (!hasSelected) {
                    alert('Select some task in order to edit.');
                }
            } else {
                alert('Type new name value!');
            }
        };

        $scope.discardChanges = function () {
            this.todos = angular.copy(this.baseStateTodos);
            this.formTodoTextModify = '';
            this.formTodoText = '';
        };
    }
);

vivify.service("todoService", function ($http, $q) {
        return ({
            addTodo: addTodo,
            getTodos: getTodos,
            removeTodo: removeTodo,
            saveTodo: saveTodo
        });
        function addTodo(data) {
            var request = $http.post("/todo", data);
            return ( request.then(handleSuccess, handleError) );
        }

        function getTodos() {
            var request = $http.get("/todo");
            return ( request.then(handleSuccess, handleError) );
        }

        function removeTodo($id) {
            var request = $http.delete("/todo/" + $id);
            return ( request.then(handleSuccess, handleError) );
        }

        function saveTodo(data, id) {
            var request = $http.put("/todo/" + id, data);
            return request.then(handleSuccess, handleError);
        }

        function handleError(response) {
            if (
                !angular.isObject(response.data) || !response.data.message
            ) {
                return ( $q.reject("An unknown error occurred.") );
            }
            return ( $q.reject(response.data.message) );
        }

        function handleSuccess(response) {
            return ( response.data );
        }
    }
);