/**
 * Created by vivify on 16.10.15..
 */

var inExampleBeforeAnonFnVar = 'value setted';
var anon = function () {
    var insideAnonFnVar = 'value setted';
    //override beforeExampleVar
    beforeExampleVar = 'value overriden by anon method';
};
var inExampleAfterAnonFnVar = 'value setted';
//debugger

//js object
var testObj = {
    firstname: 'Aleksandar',
    lastname: 'Zivkovic'
};

//js array
var testArray = [];
var i = 0;
var arrayItems = 10;
var tempObj;
while (i < arrayItems) {
    tempObj = {
        firstname: 'Firstname ' + i,
        lastname: 'Lastname ' + i
    };
    testArray.push(tempObj);
    i++;
}