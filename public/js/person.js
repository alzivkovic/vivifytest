"use strict";

class Person {
    constructor (firstname, lastname, gender) {
        this.firstname = firstname;
        this.lastname = lastname;
        Person.counter++;
    }
    //

    static getCounter(){
        return Person.counter;
    }
    static createFullName(firstname, lastname) {
        return firstname + ' ' + lastname;
    }
}
Person.counter = 0;