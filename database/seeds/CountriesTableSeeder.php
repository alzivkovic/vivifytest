<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $User_tableData = DB::table('countries_table')->get();
        App\Country::truncate();
        foreach ($User_tableData as $tableitem){
            App\Country::insert([
                'name' => $tableitem->name
            ]);
        }
    }
}
