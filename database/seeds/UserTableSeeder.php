<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $User_tableData = DB::table('users_table')->get();
        App\User::truncate();
        foreach ($User_tableData as $tableitem){
            App\User::insert([
                'email' => $tableitem->email,
                'password' => bcrypt($tableitem->password),
                'lastname' => $tableitem->lastname,
                'firstname' => $tableitem->firstname,
                'company' => $tableitem->company,
                'country_id' => $tableitem->country_id
            ]);
        }
    }
}
