<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Todos_tableData = DB::table('todos_table')->get();
        App\Todo::truncate();
        foreach ($Todos_tableData as $tableitem) {
            App\Todo::insert([
                'name' => $tableitem->name,
                'done' => $tableitem->done,
                'priority' => $tableitem->priority,
                'user_id' => $tableitem->user_id
            ]);
        }
    }
}
