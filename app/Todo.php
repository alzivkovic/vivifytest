<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    public static $validateArray = [
        'name' => 'required|max:255',
        'done' => 'required|boolean',
        'priority' => 'required|integer'
    ];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'todos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'done', 'priority', 'user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['remember_token'];

    public function user()
    {
        return $this->belongsTo('\App\User');
    }
}
